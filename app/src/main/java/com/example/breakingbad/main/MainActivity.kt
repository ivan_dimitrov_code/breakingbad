package com.example.breakingbad.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.breakingbad.R
import com.example.breakingbad.screens.details.DetailsFragment
import com.example.breakingbad.screens.error.ErrorFragment
import com.example.breakingbad.screens.list.models.app.CharacterModel
import com.example.breakingbad.screens.list.ui.CharacterListFragment
import com.example.breakingbad.utils.NetworkState

const val LAST_FRAGMENT_NAME_KEY = "LAST_FRAGMENT_NAME_KEY"

class MainActivity : AppCompatActivity(), CharacterListFragment.OnFragmentInteractionListener,
    ErrorFragment.OnFragmentInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            if (NetworkState.isConnectedToNetwork(this)) {
                showListScreen()
            } else {
                showErrorScreen()
            }
        } else {
            val fragmentTag = savedInstanceState.getString(LAST_FRAGMENT_NAME_KEY)
            fragmentTag?.let { showFragmentWithTag(it) }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(LAST_FRAGMENT_NAME_KEY, getCurrentFragment()?.tag)
        super.onSaveInstanceState(outState)
    }

    private fun showErrorScreen() {
        supportFragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        showFragmentWithTag(ErrorFragment.TAG)
    }

    private fun showListScreen() {
        showFragmentWithTag(CharacterListFragment.TAG)
    }

    private fun showDetailsFragment(item: CharacterModel) {
        val bundle = Bundle()
        bundle.putParcelable(DetailsFragment.SELECTED_CHARACTER_KEY, item)
        showFragmentWithTag(DetailsFragment.TAG, bundle)
    }

    private fun showFragmentWithTag(tag: String, bundle: Bundle? = null) {
        val fragment = provideFragmentForTag(tag, bundle)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.mainFragmentContainer, fragment!!, tag)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun provideFragmentForTag(tag: String, bundle: Bundle? = null): Fragment? {
        return if (supportFragmentManager.findFragmentByTag(tag) != null) {
            supportFragmentManager.findFragmentByTag(tag)
        } else {
            when (tag) {
                CharacterListFragment.TAG -> CharacterListFragment.newInstance()
                DetailsFragment.TAG -> bundle?.let { DetailsFragment.newInstance(it) }
                ErrorFragment.TAG -> ErrorFragment.newInstance()
                else -> Fragment()
            }
        }
    }

    override fun onBackPressed() {
        if (getCurrentFragment()?.tag == DetailsFragment.TAG) {
            supportFragmentManager.popBackStackImmediate()
            return
        }
        if (supportFragmentManager.backStackEntryCount > 1) {
            super.onBackPressed()
        }
    }

    override fun onItemClicked(item: CharacterModel) {
        showDetailsFragment(item)
    }

    override fun onError() {
        showErrorScreen()
    }

    private fun getCurrentFragment(): Fragment? {
        return if (supportFragmentManager.fragments.size > 0) {
            supportFragmentManager.fragments[supportFragmentManager.fragments.size - 1]
        } else {
            null
        }
    }

    override fun onRetryClick() {
        if (NetworkState.isConnectedToNetwork(this)) {
            showListScreen()
        }
    }
}
