package com.example.breakingbad.main

import android.app.Application
import com.example.breakingbad.screens.list.di.ListDataSourceModule
import com.example.breakingbad.screens.list.di.listRepositoryModule
import com.example.breakingbad.screens.list.di.listViewModel
import com.example.breakingbad.utils.di.apiClient
import com.example.breakingbad.utils.di.rxScheduleProvider
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@AppApplication)
            modules(
                listOf(
                    rxScheduleProvider,
                    apiClient,
                    listViewModel,
                    listRepositoryModule,
                    ListDataSourceModule
                )
            )
        }
    }
}