package com.example.breakingbad.screens.details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.breakingbad.R
import com.example.breakingbad.screens.list.models.app.CharacterModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_details.*

class DetailsFragment: Fragment(){

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate( R.layout.fragment_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val characterModel = arguments?.getParcelable<CharacterModel>(SELECTED_CHARACTER_KEY) as CharacterModel

        Picasso.get().load(characterModel.imageUrl).into(detailsImage)
        detailsName.text = characterModel.name
        detailsNickName.text = characterModel.nickName
        detailsSeasonAppearance.text = characterModel.seasonAppearance.toString()
        detailsStatus.text = characterModel.status
        detailsOcupation.text = characterModel.occupation.toString()
    }

    companion object {
        const val TAG = "DetailsFragment"
        const val SELECTED_CHARACTER_KEY = "SelectedCharacterKey"

        @JvmStatic
        fun newInstance(bundle: Bundle) =
            DetailsFragment().apply {
                arguments = bundle
            }
    }

}