package com.example.breakingbad.screens.list.models.app

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CharacterModel(
    val name: String,
    val imageUrl: String,
    val status: String,
    val occupation: List<String>,
    val seasonAppearance: List<Int>,
    val nickName: String
) : Parcelable