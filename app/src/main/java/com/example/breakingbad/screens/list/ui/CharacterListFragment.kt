package com.example.breakingbad.screens.list.ui

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.breakingbad.R
import com.example.breakingbad.screens.list.ListViewModel
import com.example.breakingbad.screens.list.models.app.CharacterModel
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class CharacterListFragment : Fragment(), CharacterListAdapter.OnCharacterListInteractionListener {
    private val viewModel by viewModel<ListViewModel>()
    private val adapter: CharacterListAdapter = CharacterListAdapter(mutableListOf(), this)
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        characterListRecyclerView.addItemDecoration(
            DividerItemDecoration(
                characterListRecyclerView.context,
                DividerItemDecoration.VERTICAL
            )
        )

        characterListRecyclerView.layoutManager = LinearLayoutManager(context)
        characterListRecyclerView.adapter = adapter
        startLiveDataObservation()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        requireActivity().menuInflater.inflate(R.menu.main_menu, menu)
        val menuItem: MenuItem = menu.findItem(R.id.action_search)
        val searchView: SearchView = menuItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun startLiveDataObservation() {
        viewModel.uiState.observe(viewLifecycleOwner, Observer {
            it?.let { state ->
                when (state) {
                    is LoadedItemsState -> enterLoadedItemsState(state.listData)
                    is LoadingState -> enterLoadingState()
                    is ErrorState -> listener?.onError()
                }
            }
        })
        viewModel.requestData()
    }

    private fun enterLoadingState(){
        characterListRecyclerView.visibility = View.INVISIBLE
        characterListProgressBar.visibility = View.VISIBLE
    }

    private fun enterLoadedItemsState(list: List<CharacterModel>){
        adapter.injectList(list)
        characterListRecyclerView.visibility = View.VISIBLE
        characterListProgressBar.visibility = View.INVISIBLE
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    interface OnFragmentInteractionListener {
        fun onItemClicked(item: CharacterModel)
        fun onError()
    }

    companion object {
        const val TAG = "CharacterListFragment"
        fun newInstance() = CharacterListFragment()
    }

    override fun onCharacterClick(characterModel: CharacterModel) {
        listener?.onItemClicked(characterModel)
    }
}
