package com.example.breakingbad.screens.list.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.breakingbad.R
import com.example.breakingbad.screens.list.models.app.CharacterModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cell_character_list.view.*
import org.koin.ext.isInt

class CharacterListAdapter(
    private val list: MutableList<CharacterModel>,
    private val listener: OnCharacterListInteractionListener
) : RecyclerView.Adapter<CharacterListAdapter.ViewHolder>(), Filterable {

    private val displayedList: MutableList<CharacterModel> = mutableListOf()

    fun injectList(newList: List<CharacterModel>) {
        list.clear()
        list.addAll(newList)
        displayedList.clear()
        displayedList.addAll(newList)
        notifyDataSetChanged()
    }

    interface OnCharacterListInteractionListener {
        fun onCharacterClick(characterModel: CharacterModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_character_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = displayedList[position]
        holder.view.setOnClickListener {
            listener.onCharacterClick(item)
        }
        Picasso.get().load(item.imageUrl).into(holder.image)
        holder.name.text = item.name
    }

    override fun getItemCount(): Int = displayedList.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.characterListName
        val image: ImageView = view.characterListImage
    }

    override fun getFilter(): Filter {
        return characterFilter
    }

    var characterFilter: Filter = object : Filter() {
        override fun performFiltering(charSequence: CharSequence): FilterResults {
            val filteredList: MutableList<CharacterModel> = ArrayList()

            if (charSequence.isEmpty()) {
                filteredList.addAll(list)
            } else {
                for (character in list) {
                    if ((charSequence.toString().isInt()
                                && filterForSeasons(character, charSequence.toString()))
                        || filterForName(character, charSequence.toString())
                    ) {
                        filteredList.add(character)
                    }
                }
            }
            val filterResults = FilterResults()
            filterResults.values = filteredList
            return filterResults
        }

        fun filterForName(characterModel: CharacterModel, name: String): Boolean {
            return characterModel.name.toLowerCase()
                .contains(name.toLowerCase())
        }

        fun filterForSeasons(characterModel: CharacterModel, season: String): Boolean {
            return characterModel.seasonAppearance.contains(season.toInt())
        }

        override fun publishResults(
            charSequence: CharSequence,
            filterResults: FilterResults
        ) {
            displayedList.clear()
            displayedList.addAll(filterResults.values as Collection<CharacterModel>)
            notifyDataSetChanged()
        }
    }
}
