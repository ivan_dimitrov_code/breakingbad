package com.example.breakingbad.screens.list

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.example.breakingbad.screens.list.data.ListRepository
import com.example.breakingbad.screens.list.ui.ErrorState
import com.example.breakingbad.screens.list.ui.ListState
import com.example.breakingbad.screens.list.ui.LoadedItemsState
import com.example.breakingbad.screens.list.ui.LoadingState
import com.example.breakingbad.utils.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class ListViewModel(
    private val repository: ListRepository,
    private val baseSchedulerProvider: BaseSchedulerProvider
) : ViewModel() {
    val uiState = MutableLiveData<ListState>()
    private val disposable = CompositeDisposable()

    fun requestData() {
        uiState.value = LoadingState(false)
        val requestDisposable = repository.getListData()
            .observeOn(baseSchedulerProvider.ui())
            .subscribeOn(baseSchedulerProvider.io())
            .subscribe({
                uiState.value = LoadedItemsState(true, it)
            }, {
                uiState.value = it.message?.let { errorMessage ->
                    ErrorState(errorMessage, false)
                }
            })
        disposable.add(requestDisposable)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun clearDisposable() {
        disposable.clear()
    }
}
