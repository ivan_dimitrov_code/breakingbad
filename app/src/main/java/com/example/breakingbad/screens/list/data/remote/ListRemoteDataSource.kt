package com.example.breakingbad.screens.list.data.remote

import com.example.breakingbad.api.ApiInterface
import com.example.breakingbad.screens.list.models.app.CharacterModel
import io.reactivex.Single

class ListRemoteDataSource(private val apiClient: ApiInterface) {

    fun requestCharacterList(): Single<List<CharacterModel>> {
        return apiClient.requestData().map {
            adaptCharacterServerToApp(it)
        }
    }
}
