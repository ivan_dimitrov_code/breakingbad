package com.example.breakingbad.screens.list.data.remote

import com.example.breakingbad.screens.list.models.app.CharacterModel
import com.example.breakingbad.screens.list.models.server.ServerCharacterModel

fun adaptCharacterServerToApp(serverModelList: List<ServerCharacterModel>): List<CharacterModel> {
    return serverModelList.map {
        CharacterModel(it.name, it.img, it.status, it.occupation, it.appearance, it.nickname)
    }
}