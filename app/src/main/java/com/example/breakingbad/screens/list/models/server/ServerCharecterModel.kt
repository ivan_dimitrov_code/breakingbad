package com.example.breakingbad.screens.list.models.server

data class ServerCharacterModel(
    val name: String,
    val img: String,
    val status: String,
    val occupation: List<String>,
    val appearance: List<Int>,
    val nickname: String
)