package com.example.breakingbad.screens.list.data

import com.example.breakingbad.screens.list.data.remote.ListRemoteDataSource
import com.example.breakingbad.screens.list.models.app.CharacterModel
import io.reactivex.Single

open class ListRepository(private val remoteDataSource: ListRemoteDataSource) {

    private var cachedList: MutableList<CharacterModel>? = null

    fun getListData(): Single<List<CharacterModel>> {
        return if (cachedList == null) {
            remoteDataSource.requestCharacterList().map {
                cachedList = it.toMutableList()
                cachedList
            }
        } else {
            Single.just(cachedList)
        }
    }

    fun clearCache() {
        cachedList?.clear()
    }
}