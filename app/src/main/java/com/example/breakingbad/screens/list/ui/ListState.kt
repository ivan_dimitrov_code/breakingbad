package com.example.breakingbad.screens.list.ui

import com.example.breakingbad.screens.list.models.app.CharacterModel

sealed class ListState {
    abstract val loadedAllItems: Boolean
}

data class LoadedItemsState(
    override val loadedAllItems: Boolean,
    val listData: List<CharacterModel>
) : ListState()

data class LoadingState(override val loadedAllItems: Boolean) : ListState()
data class ErrorState(val errorMessage: String, override val loadedAllItems: Boolean) : ListState()
