package com.example.breakingbad.screens.list.di

import com.example.breakingbad.api.ApiInterface
import com.example.breakingbad.screens.list.ListViewModel
import com.example.breakingbad.screens.list.data.ListRepository
import com.example.breakingbad.screens.list.data.remote.ListRemoteDataSource
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val listViewModel = module {
    viewModel {
        ListViewModel(get(), get())
    }
}

val listRepositoryModule = module {
    fun provideListRepository(remoteDataSource: ListRemoteDataSource): ListRepository {
        return ListRepository(remoteDataSource)
    }
    single { provideListRepository(get()) }
}

val ListDataSourceModule = module {
    fun provideListDataSourceModule(api: ApiInterface): ListRemoteDataSource {
        return ListRemoteDataSource(api)
    }
    single { provideListDataSourceModule(get()) }
}