package com.example.breakingbad.api

import com.example.breakingbad.screens.list.models.server.ServerCharacterModel
import io.reactivex.Single
import retrofit2.http.GET

interface ApiInterface {
    @GET("api/characters")
    fun requestData(): Single<List<ServerCharacterModel>>
}