package com.example.breakingbad.utils.di

import com.example.breakingbad.api.ApiClient
import com.example.breakingbad.utils.schedulers.BaseSchedulerProvider
import com.example.breakingbad.utils.schedulers.SchedulerProvider
import org.koin.dsl.module

val apiClient = module {
    single { ApiClient.getClient() }
}

val rxScheduleProvider = module {
    fun provideScheduleProvider(): BaseSchedulerProvider {
        return SchedulerProvider.instance
    }
    single { provideScheduleProvider() }
}